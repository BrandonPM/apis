console.log(" *** Get API ***");

const url = "https://rickandmortyapi.com/api/character/";

//  Obtener los datos de la API

const getData = (api) => {

    return fetch(api)
        .then((response) => response.json())
        .then((json) => {
            imprimirDatos(json)
        })
        .catch((error) => {
            console.log("Error...", error)
        })
};

let todaData;

// Imprimir los resultados 

const imprimirDatos = (data) => {
    todaData = data;
    validarPaginacion(todaData);

    let = html = "";
    data.results.forEach(matachito => {

    // Pintar los matachitos

    html += `<div class="cards">`;
    html += `<div>`;
    html += `<img class="formatoPic" src="${matachito.image}">`;
    html += `</div>`;
    html += `<div class="contenedorTextos">`;
    html += `<small class="txtLabel"> Nombre </small>`;
    html += `<p class="txtTexto">${matachito.name} </p>`;
    html += `<small class="txtLabel"> Especie </small>`;
    html += `<p class="txtTexto">${matachito.species} </p>`;
    html += `</div>`;
    html += `</div>`;
        
        //console.log("Nombre [ " + matachito.name + " ] Especie [ " + matachito.species + " ] Foto [ " + matachito.image + " ]")
    });
    document.getElementById("contenedorTodo").innerHTML = html;
}

// Paginación

const btnPrev = document.getElementById("btnPrev");
const btnNext = document.getElementById("btnNext");
const next = document.getElementById("next");
const prev = document.getElementById("prev");

btnPrev.addEventListener("click", () => {
    // Navegar hacia atrás
    // Enviar URL con ifo.prev
    getData(todaData.info.prev);
})

btnNext.addEventListener("click", () => {
    // Navegar hacia adelante
    // Enviar URL con ifo.next
    getData(todaData.info.next);
})

// Validar paginación

const validarPaginacion = (data => {
    if (data.info.prev == null) {
        btnPrev.style.display = (btnPrev.style.display == 'none') ? 'block' : 'none'; 
        console.log("No se puede retorceder.");
    } else {
        btnPrev.style.display = (btnPrev.style.display == 'inline') ? 'block' : 'inline'; 
        console.log("Sí se puede retroceder.");
    }

    if (data.info.next == null) {
        btnNext.style.display = (btnNext.style.display == 'none') ? 'block' : 'none'; 
        console.log("No se puede avanzar.");
    } else {
        btnNext.style.display = (btnNext.style.display == 'inline') ? 'block' : 'inline'; 
        console.log("Sí se puede avanzar.");
    }
})

// Consumir la API

getData(url);

// let = html = "";

// for (let i = 1; i <= 10; i++) {

//     html += `<div class="cards">`;
//     html += `<div>`;
//     html += `<img class="formatoPic" src="ironman.png"`;
//     html += `</div>`;
//     html += `<div class="contenedorTextos">`;
//     html += `<small class="txtLabel"> Nombre </small>`;
//     html += `<p class="txtTexto">Tony Stark </p>`;
//     html += `<small class="txtLabel"> Héroe </small>`;
//     html += `<p class="txtTexto">Iron Man </p>`;
//     html += `</div>`;
//     html += `</div>`;
// }

// document.getElementById("contenedorTodo").innerHTML = html;