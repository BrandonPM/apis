console.log(" *** Get API ***");

const url = "https://pokeapi.co/api/v2/pokemon/";

/*  ?offset=00&limit=10 
    Establecer un límite de matachitos
*/

//  Obtener los datos de la API

const getData = (api, opc) => {

    return fetch(api)
        .then((response) => response.json())
        .then((json) => {

            if (opc == 0) 
                obtenerUrl(json);
            else 
                imprimirDatos(json);

        })
        .catch((error) => {
            console.log("Error...", error)
        })
};

let todaData;
let html;

const obtenerUrl = (data) => {

    todaData = data;
    validarPaginacion(todaData);
    data.results.forEach(pokemon => {
        html = "";
        getData(pokemon.url, 1);
    });
}

// Imprimir los resultados 

const imprimirDatos = (data) => {

    // Pintar los pokemones

    html += `<div class="cards">`;
    html += `<div>`;
    html += `<img class="formatoImg" src="${data.sprites.other.dream_world.front_default}">`;
    html += `</div>`;
    html += `<div class="contenedorTextos">`;
    html += `<small class="txtLabel"> Nombre </small>`;
    html += `<p class="txtTexto">${data.name} </p>`;
    html += `<small class="txtLabel"> Habilidades </small>`;

    data.abilities.forEach(habilidad => {
        html += `<p class="txtTexto">${habilidad.ability.name} </p>`;
    });

    html += `</div>`;
    html += `</div>`;
        
    document.getElementById("contenedorTodo").innerHTML = html;
}

// Paginación

const btnPrev = document.getElementById("btnPrev");
const btnNext = document.getElementById("btnNext");
const next = document.getElementById("next");
const prev = document.getElementById("prev");

btnPrev.addEventListener("click", () => {
    // Navegar hacia atrás
    // Enviar URL con ifo.prev
    getData(todaData.previous, 0);
})

btnNext.addEventListener("click", () => {
    // Navegar hacia adelante
    // Enviar URL con ifo.next
    getData(todaData.next, 0);
})

// Validar paginación

const validarPaginacion = (data => {
    if (data.previous == null) {
        btnPrev.style.display = (btnPrev.style.display == 'none') ? 'block' : 'none'; 
        console.log("No se puede retorceder.");
    } else {
        btnPrev.style.display = (btnPrev.style.display == 'inline') ? 'block' : 'inline'; 
        console.log("Sí se puede retroceder.");
    }

    if (data.next == null) {
        btnNext.style.display = (btnNext.style.display == 'none') ? 'block' : 'none'; 
        console.log("No se puede avanzar.");
    } else {
        btnNext.style.display = (btnNext.style.display == 'inline') ? 'block' : 'inline'; 
        console.log("Sí se puede avanzar.");
    }
})

// Consumir la API

getData(url, 0);

// let = html = "";

// for (let i = 1; i <= 10; i++) {

//     html += `<div class="cards">`;
//     html += `<div>`;
//     html += `<img class="formatoPic" src="ironman.png"`;
//     html += `</div>`;
//     html += `<div class="contenedorTextos">`;
//     html += `<small class="txtLabel"> Nombre </small>`;
//     html += `<p class="txtTexto">Tony Stark </p>`;
//     html += `<small class="txtLabel"> Héroe </small>`;
//     html += `<p class="txtTexto">Iron Man </p>`;
//     html += `</div>`;
//     html += `</div>`;
// }

// document.getElementById("contenedorTodo").innerHTML = html;